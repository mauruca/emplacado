#(c) 2015 Ultima Ratio Regis, All Rights Reserved
#!/usr/bin/python

"""
>>> python server.py 8002 consume "http://1.1.1.1:8080/report"
serving on 127.0.0.1:8002

>>> python server.py 8001 keep
serving on 127.0.0.1:8001

>>> python server.py 8001
serving on 127.0.0.1:8001

or simply

>>> python server.py
Serving on localhost:8000

You can use this to test GET and POST methods.

"""

import SimpleHTTPServer
import SocketServer
import logging
import cgi
import json
import sys
import time
import signal
import sys

I = ""
server = 0
URLBUSINESSWS = ""
QUEUE = "keep"

if len(sys.argv) > 3:
    PORT = int(sys.argv[1])
    URLBUSINESSWS = sys.argv[3]
    QUEUE = sys.argv[2]
elif len(sys.argv) > 2:
    PORT = int(sys.argv[1])
    QUEUE = sys.argv[2]
elif len(sys.argv) > 1:
    PORT = int(sys.argv[1])
else:
    PORT = 8000


class ServerHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def do_GET(self):
        logging.warning("======= GET STARTED =======")
        logging.warning(self.headers)
        SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

    def do_POST(self):
        logging.warning("======= POST STARTED =======")
        logging.warning(self.headers)
        logging.warning("======= POST VALUES =======")
        #Translate data
        trans = Translate()
        content_len = int(self.headers.getheader('content-length'))
        soapmes = trans.ReadEmplacadoData(self.rfile.read(content_len))

#        if(QUEUE!="consume"):
#            self.send_error(500,"Data was not processed, keep data in queue...")
#        else:
        self.send_response(200)

        self.end_headers()
        logging.warning("======= POST FINISHED =======")

        logging.warning("======= POST TO BUSINESS STARTED =======")
        #Send to business server
        bus = BusinessServer()
        bus.Post2Business(soapmes)
        logging.warning("======= POST TO BUSINESS FINISHED =======")

        print("\n")

class Translate():

    def ConvertEpoch2String(self,epoch_dt):
        epoch_dt = float(epoch_dt)/1000.
        ts = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(epoch_dt))
        return ts

    def BuildSoapMessage(self,data):
        splitchar = '-'

        #Defining data
        site_id = data['site_id']
        if(site_id.find(splitchar)!=-1):
            posto = data['site_id'].split(splitchar)[0]
            solicitante = data['site_id'].split(splitchar)[1]
        else:
            posto = site_id
            solicitante = site_id

        placa = data['results'][0]['plate']
        #Converting from epoch to human readable format
        ts = self.ConvertEpoch2String(data['epoch_time'])
        #Build message
        message = "<posto>%s</posto><solicitante>%s</solicitante><placa>%s</placa><dataDoEvento>%s</dataDoEvento>" % (posto,solicitante,placa,ts)
        return message

    def ReadEmplacadoData(self,jsonData):

        SM_TEMPLATE = """<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Body>
        <ns1:readLS xmlns:ns1="http://phonedirlux.homeip.net/types">%s</ns1:readLS>
        </SOAP-ENV:Body></SOAP-ENV:Envelope>"""

        self.data = json.loads(jsonData)

        #Build message
        message = self.BuildSoapMessage(self.data)
        #Build soap
        SoapMessage = SM_TEMPLATE%(message)

        return SoapMessage

class BusinessServer():

    def Post2Business(self,sm):

        if( len(URLBUSINESSWS) > 0 ):
            request = urllib2.Request(URLBUSINESSWS)
            request.add_header('Content-Type', 'application/xml')
            response = urllib2.urlopen(request, sm)        
        else:
            print("console post: %s") % sm 

if __name__ == '__main__':
    Handler = ServerHandler
    server = SocketServer.TCPServer(("", PORT), Handler)
    print "(c) 2015 Ultima Ratio Regis - EmplacadoConector server version 1.0 - tech@ur2.com.br"
    print "Serving at: http://%(interface)s:%(port)s" % dict(interface=I or "localhost", port=PORT)
    print "Queue action: %s" % (QUEUE)
    print "Translating and forwarding data to: %(urlbusinesswsserver)s" % dict(urlbusinesswsserver=URLBUSINESSWS or "console")
    print "Press Ctrl+C to stop...server running."

    try:
        server.serve_forever()

    except KeyboardInterrupt:
        print('Stoping the server...')
        server.shutdown()
        server.server_close()
        sys.exit(0)


