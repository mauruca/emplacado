#!/bin/bash

if [ "$FLAVOR" = "prod" ] ; then
  echo "Configuring the system..."
  eval "echo -E \"$(< $1)\"" > $1
  echo "Configuration finished."
fi
