#!/bin/bash

cd ../../

git clone https://github.com/openalpr/openalpr.git

mkdir ./openalpr/src/build

cp -R ./emplacado/emplacadocontainer/config/ ./openalpr/
cp -R ./emplacado/emplacadocontainer/runtime_data/ ./openalpr/

cd ./openalpr/src/build

cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr -DCMAKE_INSTALL_SYSCONFDIR:PATH=/etc ..

make

make install
