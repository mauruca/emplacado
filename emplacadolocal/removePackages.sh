#!/bin/bash

sudo apt-get remove libopencv-dev libtesseract-dev git cmake build-essential libleptonica-dev
sudo apt-get remove liblog4cplus-dev libcurl3-dev
sudo apt-get remove beanstalkd
